package br.com.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
// ja traz as configurações de integração com o Eureka
@EnableZuulProxy
// opera sem nenhuma configuração, ou seja, vc precisa configurar tudo
// @EnableZuulServer
public class ZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulApplication.class, args);
	}

}

